import React from 'react';
import styled, { keyframes } from 'styled-components';

import T from '/imports/ui/elements/typography';

const loaderSizes = {
    s: 24,
    m: 40,
    l: 80,
};

const keyframe = keyframes`
0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  margin: 0 auto;
`;

const Label = styled(T.Text)`
  margin-top: ${({ theme }) => theme.spaces.m}px;
`;

const Outer = styled.div`
    position: relative;
    width: ${({ size }) => loaderSizes[size]}px;
    height: ${({ size }) => loaderSizes[size]}px;
    margin: 0 auto;
`;

const Inner = styled.div`
    display: block;
    width: ${({ size }) => loaderSizes[size] / 1.8}px;
    height: ${({ size }) => loaderSizes[size] / 1.8}px;
    border: ${({ size }) => loaderSizes[size] / 5}px solid ${({ theme }) => theme.colors.text};
    border-color: ${({ theme }) => theme.colors.text} transparent ${({ theme }) => theme.colors.text} transparent;
    animation: ${keyframe} 1.2s linear infinite;
    border-radius: 50%;
`;

export default Loader = ({ size = 'm', label }) => (
    <Wrapper>
        <Outer size={size}>
            <Inner size={size} />
        </Outer>
        {label ? <Label>{label}</Label> : null}
    </Wrapper>
)