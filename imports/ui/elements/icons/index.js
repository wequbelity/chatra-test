import UnicornIcon from './UnicornIcon';
import CopyIcon from './CopyIcon';

export default {
    unicorn: UnicornIcon,
    copy: CopyIcon,
}