import React from 'react';
import styled from 'styled-components';

import T from '/imports/ui/elements/typography';
import icons from '/imports/ui/elements/icons';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  margin: 32px auto;
`;

const Label = styled(T.Text)`
  margin-top: ${({ theme }) => theme.spaces.m}px;
`;

const Text = styled(T.Text)`
  margin-top: ${({ theme }) => theme.spaces.s}px;
  font-size: ${({ theme }) => theme.fontSizeS}px;
`;

const UnicornIcon = styled(icons.unicorn)`
    fill: ${({ theme }) => theme.colors.text};
    width: 60px;
`

export default NoData = ({ label, text}) => (
    <Wrapper>
        <UnicornIcon />
        {label ? <Label>{label}</Label> : null}
        {text ? <Text>{text}</Text> : null}
    </Wrapper>
)
