import React from 'react';
import styled from 'styled-components';

const H1 = styled.h1`
  font-size: ${({theme}) => theme.fontSizeXL}px;
  color: ${({theme}) => theme.colors.light};
  text-align: center;
  font-weight: 700;

  @media(max-width: 480px) {
    font-size: ${({theme}) => theme.fontSizeL}px;
  }
`;

const H2 = styled.h2`
  font-size: ${({theme}) => theme.fontSizeL}px;
  color: ${({theme}) => theme.colors.light};
  text-align: center;
  font-weight: 400;

  @media(max-width: 480px) {
    font-size: ${({theme}) => theme.fontSizeM}px;
  }
`;

export default ({children, className, level = 1, ...props}) => {
    let Element = H1;
    
    switch(level) {
        case 2: Element = H2;
    }

    return <Element {...props} className={className}>{children}</Element> 
};