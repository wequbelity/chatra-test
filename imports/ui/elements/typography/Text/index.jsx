import React from 'react';
import styled from 'styled-components';

const Text = styled.span`
  font-size: ${(props) => props.theme.fontSizeM};
  color: ${({theme}) => theme.colors.text};
`;

export default ({children, ...props}) => <Text {...props}>{children}</Text>;