import React from 'react';
import styled from 'styled-components';
import copy from 'copy-to-clipboard';

import icons from '/imports/ui/elements/icons';

const Button = styled.button`
    border: none;
    outline: none;
    background: transparent;
    display: flex;
    align-items: center;
`;
const CopyIcon = styled(icons.copy)`
  width: 28px;
  height: 28px;
  fill: white;
  cursor: pointer;
`;

export default CopyURL = ({ className }) => {
    const onClick = () => { 
        copy(window.location.href);
        alert('The link has been copied!');
    };

    return (
        <Button className={className} onClick={onClick}>
            <CopyIcon />
        </Button>
    )
}