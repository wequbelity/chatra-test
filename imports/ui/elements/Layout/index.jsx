import React from 'react';
import styled from 'styled-components';

const Main = styled.main`
    background: ${({theme}) => theme.colors.bgLight};
    background: linear-gradient(180deg, ${({theme}) => theme.colors.bgLight} 0%, ${({theme}) => theme.colors.bg} 100%);
    width: 100%;
`

const Content = styled.section`
    padding: 0 ${({theme}) => theme.spaces.m}px ${({theme}) => theme.spaces.m}px ${({theme}) => theme.spaces.m}px;
    padding-top: ${({theme}) => theme.spaces.xl}px; 
    max-width: ${({theme}) => theme.contentWidth}px;
    min-height: 100vh;
    width: 100%;
    box-sizing: border-box;
    margin: 0 auto; 
`

export default ({children}) => <Main><Content>{children}</Content></Main>