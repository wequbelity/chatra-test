import React from 'react';
import styled, {ThemeProvider} from 'styled-components';

import Accounts from '/imports/ui/components/Accounts';
import Games from '/imports/ui/components/Games';
import Layout from '/imports/ui/elements/Layout';
import T from '/imports/ui/elements/typography';
import CopyURL from '/imports/ui/elements/CopyUrl';
import Store, {StoreContext} from '/imports/ui/store';

import './styles/global.css';
import './styles/reset.css';

const theme = {
  contentWidth: 800,

  fontSizeS: 12,
  fontSizeM: 16,
  fontSizeL: 22,
  fontSizeXL: 32,

  colors: {
    white: 'white',
    light: 'white',
    text: 'rgba(83,87,91,1)',
    bg: 'rgba(18,19,20,1)',
    bgLight: 'rgba(43,47,51,1)',
  },

  spaces: {
    s: 8,
    m: 16,
    l: 24,
    xl: 48,
  }
};

const AccountsContainer = styled(Accounts)`
  margin-top: ${({ theme }) => theme.spaces.l}px;
`;
const GamesContainer = styled(Games)`
  margin-top: ${({ theme }) => theme.spaces.l}px;
`;
const Header = styled.header`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledCopyURL= styled(CopyURL)`
  margin-left: ${({ theme }) => theme.spaces.m}px;
`;

export default App = () => (
  <ThemeProvider theme={theme}>
    <StoreContext.Provider value={new Store}>
      <Layout>
        <Header>
          <T.Title>Let's steam together</T.Title>
          <StyledCopyURL />
        </Header>
        <AccountsContainer />
        <GamesContainer />
      </Layout>
    </StoreContext.Provider>
  </ThemeProvider>
);
