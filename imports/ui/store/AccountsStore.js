import {makeObservable, action, computed, observable, reaction} from 'mobx';

import Account from './AccountStore';

function checkSetEqaul(set1, set2) {
    if (set1.size !== set2.size) {
        return false;
    }

    for (const id of set1) {
        if (!set2.has(id)) {
            return false;
        }
    }

    return true;
}

class AccountsStore {
    root = null;
    accounts = [];

    constructor(root) {
        this.root = root;

        makeObservable(this, {
            accounts: observable,
            approvedAccounts: computed,
            approvedAccountsIds: computed({equals: checkSetEqaul}),
            add: action,
            deleteOne: action,
        });

        this.registerUrlSync();
    }

    registerUrlSync() {
        const params = new URLSearchParams(location.search);
        if (params.get('accounts')) {
            params.get('accounts').split(',').map(accountId => {
                this.add(accountId);
            })
        }

        reaction(() => this.approvedAccountsIds, (accounts) => {
            window.history.replaceState(null, '', `?accounts=${[...accounts].join(',')}`)
        });
    }

    get approvedAccounts() {
        return this.accounts.filter(acc => acc.approved);
    }

    get approvedAccountsIds() {
        return new Set(this.approvedAccounts.map(acc => acc.id));
    } 

    add = (identifier) => {
        this.accounts.push(Account.create(identifier, this))
    }

    deleteOne = (id) => {
        this.accounts.splice(this.accounts.findIndex(acc => acc.id === id), 1);
    }
}

export default AccountsStore;