import { Meteor } from 'meteor/meteor';
import {makeObservable, action, observable} from 'mobx';

const DEFAULT_AVATAR = 'https://play-lh.googleusercontent.com/XtyURW0mKNnKu_6TzQ5_WpuKF4A7M1oFV6p828eVEWIvTZPtZz2gq5sNM78jpNPMMRmZ';

class AccountStore {
    parentStore = null;

    name = '';
    avatar = DEFAULT_AVATAR;
    id = null;
    link = '';
    approved = false;

    constructor({name, avatar = DEFAULT_AVATAR, id = Math.random(), link}, parentStore = null) {
        makeObservable(this, {
            name: observable,
            avatar: observable,
            id: observable,
            link: observable,
            approved: observable,
            delete: action,
        })

        this.name = name;
        this.avatar = avatar;
        this.id = id;
        this.link = link;

        this.parentStore = parentStore;
    }

    delete = () => {
        this.parentStore.deleteOne(this.id);
    }
}

function parseIdentifier(identifier) {
    if (identifier.indexOf('steamcommunity.com/id/') > -1) {
        return identifier.split('/').slice(-1)[0];
    }

    return identifier;
}

AccountStore.create = function(rawIdentifier, parent) {
    const identifier = parseIdentifier(rawIdentifier);
    const account =  new AccountStore({name: identifier}, parent);

    Meteor.call('accounts.get', {identifier}, (err, res) => {
        if (err) {
            account.delete();
            alert('Account not found!');
        } else if (parent.approvedAccountsIds.has(res.id)) {
            account.delete();
            alert('Account already exists!');
        } else {
            account.id = res.id;
            account.link = res.link;
            account.name = res.name;
            account.avatar = res.avatar;
            account.approved = true;
        }
    });

    return account;
}

export default AccountStore;