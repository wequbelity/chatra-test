import React, {useContext} from 'react';

import GamesStore from './GamesStore';
import AccountsStore from './AccountsStore';

class Store {
    accountsStore = new AccountsStore(this);
    gamesStore = new GamesStore(this);
}

export const StoreContext = React.createContext();
/**
 * @returns {Store}
 */
export const useStore = () => useContext(StoreContext);
export default Store;