import {makeObservable, action, reaction, observable, computed} from 'mobx';

import {createMeteorCallLatest} from '/imports/ui/utils';

const meteorCallLatest = createMeteorCallLatest();

class GamesStore {
    root = null;
    games = [];
    loading = false;

    constructor(root) {
        this.root = root;

        makeObservable(this, {
            games: observable,
            gamesList: computed,
            loading: observable,
            fetch: action,
        })

        reaction(() => this.root.accountsStore.approvedAccountsIds, this.fetch);
    }

    get gamesList() {
        return this.games.slice().sort((a, b) => {
            if (a.title === b.title) {
                return 0;
            } else if (a.title < b.title) {
                return -1;
            }

            return 1;
        })
    }

    fetch = () => {
        const accounts = Array.from(this.root.accountsStore.approvedAccountsIds);

        this.loading = true;
        this.games.replace([]);

        meteorCallLatest('games.get', {accounts}, (err, res) => {
            if (!err) {
                this.games.replace(res);
            } else {
                console.error(err);
            }

            this.loading = false;
        })
    }
}

export default GamesStore;