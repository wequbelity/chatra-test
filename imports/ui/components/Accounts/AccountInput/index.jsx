import React, {useRef} from 'react';
import styled from 'styled-components';

const Wrapper = styled.form`
    display: flex;
    align-items: center;
`;

const Input = styled.input`
    background-color: transparent;
    color: ${({theme}) => theme.colors.light};
    font-size: 32px;
    border: none;
    border-radius: 0;
    border-bottom: 1px solid ${({theme}) => theme.colors.light};
    outline: none;
    padding: 4px 8px;

    @media (max-width: 480px) {
       font-size: 18px;
    }
    @media (max-width: 360px) {
        font-size: 16px;
     }
`;

const Button = styled.button`
    flex-shrink: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: transparent;
    border: 1px solid ${({theme}) => theme.colors.light};
    color: ${({theme}) => theme.colors.light};
    border-radius: 50%;
    width: 32px;
    height: 30px;
    font-size: 20px;
    line-height: 1;
    margin-left: ${({theme}) => theme.spaces.m}px;

    &:hover {
        background-color: white;
        color: black;
        cursor: pointer;
    }

    @media (max-width: 480px) {
        width: 24px;
        height: 24px;
        font-size: 12px;
    }
`;

export default AccountInput = ({onSubmit}) => {
    const input = useRef(null);

    const onFormSubmit = (e) => {
        onSubmit(input.current.value);
        
        input.current.value = '';

        e.preventDefault();
    }
    
    return (
        <Wrapper onSubmit={onFormSubmit}>
            <Input ref={input} placeholder="Add account" />
            <Button type="submit">{'+'}</Button>
        </Wrapper>
    )
};