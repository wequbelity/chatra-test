import React from 'react';
import {observer} from 'mobx-react-lite';
import styled from 'styled-components';

import Account from '../Account';

const Wrapper = styled.section`
    margin-top: ${({theme}) => theme.spaces.l}px;
    width: 100%;
    display: flex;
    flex-wrap: wrap;
`;

export default AccountsList = observer(({accounts = []}) => {
    return (
        <Wrapper>
            {accounts.map(account => <Account key={account.id} account={account} />)}
        </Wrapper>
    )
});