import React from 'react';
import styled from 'styled-components';

import {useStore} from '/imports/ui/store';

import AccountInput from './AccountInput';
import AccountsList from './AccountsList';

const Wrapper = styled.section`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export default Accounts = ({ className }) => {
    const {accountsStore} = useStore();

    return (
        <Wrapper className={className}>
            <AccountInput onSubmit={accountsStore.add} />
            <AccountsList accounts={accountsStore.accounts} />
        </Wrapper>
    );
};