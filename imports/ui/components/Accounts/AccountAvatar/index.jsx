import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
    width: 24px;
    height: 24px;
    border-radius: 50%;
    overflow: hidden;
`;

const Image = styled.img`
    width: 100%;
    height: 100%;
    object-fit: center;
`

export default AccountAvatar = ({src}) => (
    <Wrapper>
        <Image src={src} />
    </Wrapper>
)