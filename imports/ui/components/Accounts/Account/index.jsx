import React from 'react';
import styled from 'styled-components';
import { observer } from 'mobx-react-lite';

import Loader from '/imports/ui/elements/Loader';

import AccountAvatar from '../AccountAvatar';

const Wrapper = styled.div`
    background-color: ${({ theme }) => theme.colors.white};
    opacity: ${({ loading }) => loading ? 0.75 : 1};
    border: none;
    border-radius: 14px;
    padding: 4px 12px;
    display: flex;
    align-items: center;
    cursor: ${({ loading }) => loading ? 'not-allowed' : 'pointer'};
    margin-bottom: ${({ theme }) => theme.spaces.s}px;
    margin-right: ${({ theme }) => theme.spaces.s}px;
`;

const AccountLink = styled.a`
    margin-right: ${({ theme }) => theme.spaces.l}px;
    display: flex;
    align-items: center;
    color: inherit;
    text-decoration: none;

    &:hover {
        text-decoration: underline;
    }
`;

const DeleteButton = styled.button`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 22px;
    height: 22px;
    border-radius: 50%;
    cursor: pointer;
    outline: none;
`;

const Name = styled.span`
    margin-left: ${({ theme }) => theme.spaces.s}px;
`;

export default Account = observer(({ account }) => (
    <Wrapper loading={!account.approved}>
        <AccountLink href={account.link} target="_blank">
            <AccountAvatar src={account.avatar} />
            <Name>{account.name}</Name>
        </AccountLink>
        {account.approved
            ? <DeleteButton onClick={account.delete} dangerouslySetInnerHTML={{__html: `&#10005`}} />
            : <Loader size="s" />
        }
    </Wrapper>
));