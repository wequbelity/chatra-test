import React from 'react';
import styled from 'styled-components';
import {observer} from 'mobx-react-lite';

import GameCard from '../GameCard';

const Wrapper = styled.section`
    grid-gap: 32px;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    
    @media (max-width: 764px) {
        grid-template-columns: 1fr 1fr;
    }

    @media (max-width: 480px) {
        grid-template-columns: 1fr;
    }
`;

export default GamesList = observer(({games = []}) => (
    <Wrapper>
        {games.map(game => <GameCard key={game.id} game={game} />)}
    </Wrapper>
));