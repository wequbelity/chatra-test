import React from 'react';
import styled from 'styled-components';
import { observer } from 'mobx-react-lite';

import T from '/imports/ui/elements/typography';
import Loader from '/imports/ui/elements/Loader';
import { useStore } from '/imports/ui/store';

import GamesList from './GamesList';
import GamesNoData from './GamesNoData';

const Wrapper = styled.section``;
const Title = styled(T.Title)`
    margin-bottom: ${({ theme }) => theme.spaces.m}px;
`

export default Games = observer(({ className }) => {
    const { gamesStore } = useStore()

    const gamesElement = gamesStore.gamesList.length === 0 
        ? <GamesNoData />
        : <GamesList games={gamesStore.gamesList} />;

    const contentElement = gamesStore.loading 
        ? <Loader label="Loading common games..." /> 
        : gamesElement;

    return (
        <Wrapper className={className}>
            <Title level={2}>Common games</Title>
            {contentElement}
        </Wrapper>
    );
});