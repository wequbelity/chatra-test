import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.a`
    width: 100%;
    overflow: hidden;
    color: inherit;
    text-decoration: none;
`;
const Container = styled.div`
    position: relative;
    padding-top: 47%;
    overflow: hidden;
`;
const GamePreview = styled.img`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    object-fit: cover;
    background-color: #aeaeae;
    transition: transform 0.3s ease-in-out;

    ${Wrapper}:hover & {
        transform: scale(1.05);
    }
`;
const GameTitle = styled.h2`
    background-color: white;
    bottom: 0;
    width: 100%;
    padding: 8px;
    box-sizing: border-box;
    min-height: 48px;
    display: flex;
    align-items: center;

    ${Wrapper}:hover & {
        text-decoration: underline;
    }
`;

export default GameCard = ({game: {title, preview, link}}) => (
    <Wrapper href={link} target="_blank">
        <Container>
            <GamePreview src={preview} alt={`Preview for ${title}`}/>
        </Container>
        <GameTitle>{title}</GameTitle>
    </Wrapper>
);