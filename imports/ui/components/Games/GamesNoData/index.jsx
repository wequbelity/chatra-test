import React from 'react';
import {observer} from 'mobx-react-lite';

import { useStore } from '/imports/ui/store';
import NoData from '/imports/ui/elements/NoData';

export default GamesNoData = observer(function GamesNoData() {
    const { accountsStore } = useStore();

    if (accountsStore.approvedAccounts.length < 2) {
        return <NoData label="Not enough accounts!"  text="Try to add accounts. We need a minimum of two."/>;
    }

    return <NoData label="Common games not found!"  text="Try to change accounts."/>;
});