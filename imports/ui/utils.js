export function createMeteorCallLatest() {
    let latest = null;
    
    return function(method, params, callback) {
        latest = callback;

        Meteor.call(method, params, (err, res) => {
            if (latest === callback) {
                callback(err, res);
            }
        })
    }
}