import { Meteor } from 'meteor/meteor';

import {getAccount, getGames} from './methods';

Meteor.startup(() => {
    Meteor.methods({
        'accounts.get': getAccount,
        'games.get': getGames,
      });
});
