import { set } from 'mobx';
import * as api from './api';

const MULTIPLAYER_GAMES_CATEGORIES_IDS = [1, 9, 49, 24];
const isMultiplayerGame = game => {
    if (!game || !game.categories) {
        return false;
    }

    for (const categoryId of MULTIPLAYER_GAMES_CATEGORIES_IDS) {
        for (const category of game.categories) {
            if (category.id === categoryId) {
                return true;
            }
        }
    }

    return false;
};

function intersection(sets = []) {
    const intersectionSet = new Set(sets[0]);
    
    for (const value of intersectionSet) {
        for (set of sets) {
            if (!set.has(value)) {
                intersectionSet.delete(value);
            }
        }
    }

    return intersectionSet;
}
    

export async function getAccount({ identifier }) {
    const account = await api.getAccount({identifier});
    
    if (!account) {
        throw new Meteor.Error('account-not-found', "Can't find account");
    }

    return {
        id: account.steamid,
        link: account.profileurl,
        name: account.personaname,
        avatar: account.avatarmedium,
    }
}

export async function getGames({ accounts = []}) {
    if (accounts.length < 2) {
        throw new Meteor.Error('games-cant-get', "Not enough accounts for getting games (min: 2)");
    }

    const accountsOwnedGames = await api.getAccountsOwnedGames({accountsIds: accounts});
    const accountsOwnedGamesIdsSets = accountsOwnedGames.map(
        accountOwnedGames => new Set(accountOwnedGames?.map(game => game.appid) || [])
    );
    const commonGamesIds = Array.from(intersection(accountsOwnedGamesIdsSets));
    const games = await Promise.all(
        commonGamesIds.map((id) => api.getGameInfo({gameId: id}))
    );
    
    const multiplayerGames = games.filter(isMultiplayerGame);

    return multiplayerGames;
}