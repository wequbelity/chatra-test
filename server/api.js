import fetch from 'node-fetch';

import {createLRU, createThrottle} from './utils';

const steamJSONFetch = createThrottle(
    (...rest) => fetch(...rest).then(res => res.json()),
    {requestsPerSecond: 5}
);

const steamJSONThrottleFetch = createLRU(steamJSONFetch, { 
    max: 10000, 
    maxAge: 1000 * 60 * 60 
})

const {STEAM_KEY} = Meteor.settings;

export async function resolveAccountIdentifier({identifier}) {
    const resolveIdUrl = `https://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=${STEAM_KEY}&vanityurl=${identifier}`;
    const {response: resolveResponse} = await fetch(resolveIdUrl).then(res => res.json());

    return resolveResponse.success === 1 ? resolveResponse.steamid : null;
}

export async function getAccountSummary({accountId}) {
    const userSummaryUrl = `https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=${STEAM_KEY}&steamids=${accountId}`;
    const {response: summaryResponse} = await fetch(userSummaryUrl).then(res => res.json());
    
    const account = summaryResponse?.players[0];

    return account;
}

export async function getAccount({identifier}) {
    const accountId = await resolveAccountIdentifier({identifier});
    const account = await getAccountSummary({
        accountId: accountId || identifier
    });

    return account;
}

export async function getAccountsOwnedGames({accountsIds}) {
    return await Promise.all(accountsIds.map(accountId => {
        const getOwnedGamesUrl = `https://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=${STEAM_KEY}&steamid=${accountId}&format=json`;
        
        return fetch(getOwnedGamesUrl).then(res => res.json()).then(json => json.response.games);
    }))
}

export async function getGameInfo({gameId}) {
    const getGameInfoUrl = `https://store.steampowered.com/api/appdetails/?appids=${gameId}`;
    
    try {
        const response = await steamJSONThrottleFetch(getGameInfoUrl);
        const gameInfo = response[gameId]?.data;

        return {
            id: gameInfo?.steam_appid,
            preview: gameInfo?.header_image || gameInfo?.screenshots[0]?.path_full,
            title: gameInfo?.name,
            categories: gameInfo?.categories || [],
            link: `https://store.steampowered.com/app/${gameInfo?.steam_appid}`
        };
    } catch(e) {
        console.error('ERROR (API)', getGameInfoUrl, e);

        return {
            id: null,
            categories: []
        };
    }
}