import LRU from 'lru-cache';
import PromiseThrottle from 'promise-throttle';

export function createThrottle(func, {requestsPerSecond, promiseImplementation = Promise}) {
    const promiseThrottle = new PromiseThrottle({requestsPerSecond, promiseImplementation});

    return function(...rest) {
        return promiseThrottle.add(function() {
            return func(...rest);
        });
    }
}

export function createLRU(func, options) {
    const cache = new LRU(options);

    return async function(...rest) {
        const key = rest.join('___');

        if (cache.has(key)) {
            return cache.get(key);
        }

        const result = await func(...rest);

        cache.set(key, result);

        return result;
    }
}