# Overview
## Stack
- Meteor
- React
- Mobx
- Styled-components
## Structure
- `client`
    - `main.jsx` – enter point for client part
    - `main.html` – HTML template for web clients
- `server`
    - `api.js` – communication with 3rd party (steam)
    - `main.js` – enter point for server part
    - `methods.js` – server RPC methods
    - `utils.js` – server helping functions
- `imports`
    - `ui`
        - `components` – feature based components
        - `elements` – isolated components (common for all client features)
        - `store` – mobx stores
        - `styles` – global styles
        - `App.jsx` – main React component
        - `utils.js` – client helping functions

# Running
## Dev
Install dependencies
```bash
meteor npm install
```

Running dev server (MONGO_URL=mongodb://localhost:27017/chatra-test)
```bash
npm run start -- --settings development.json
```

# Configuration
## Environment variables 
- `MONGO_URL` – connection string for mongodb (example: mongodb://localhost:27017/chatra-test)
## Meteor settings
```json
{
    "STEAM_KEY": "{{API_KEY_FOR_STEAM}}"
}
```